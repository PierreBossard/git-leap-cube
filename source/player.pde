public class player{
  float xPlayer,yPlayer,ySpeed, xSpeed;
  int rectWidth,rectHeight, health, stopwatch;
  int time = millis();
  
player(){
  xPlayer = 250;
  yPlayer = 350;
  rectWidth = 50;
  rectHeight = 50;
  xSpeed = 3;
  health = 1;
  stopwatch = 0;
}

void drawPlayer(){
  stroke(191, 215, 44);
  fill(191, 215, 44);
  strokeWeight(2);
  rect(xPlayer,yPlayer,rectWidth,rectHeight);
  fill(184, 207, 43);
  rect(xPlayer + 10,yPlayer + 10,rectWidth - 20,rectHeight - 20);
}

void jump(){
  ySpeed= -7; 
}

void drag(){
  ySpeed+=0.3; 
}

void move(){ 
  
  yPlayer += ySpeed;

  for(int i = 0; i<3 ; i++){
  w[i].xWall-= xSpeed;
  }

  for(int j = 0; j<8 ; j++){
  c[j].xCoin-= xSpeed; //speed of the wall
  }
}

void jumpInverse(){
  ySpeed= +10; 
}

void dragInverse(){
  ySpeed-=0.3; 
}

void moveInverse(){ 
  
  yPlayer += ySpeed;

  for(int i = 0; i<3 ; i++){
  w[i].xWall-= xSpeed;
  }

  for(int j = 0; j<8 ; j++){
  c[j].xCoin-= xSpeed; //speed of the coin
  }
}

void dead(){
    ySpeed = 20;

    if (health > 1){
     state = "rv"; 
     health -= 1;
     yPlayer += 50;
    }
    else if (health == 1){
          state = "r";
    addNewScore(stopwatch); 
    }
    looseHealth.rewind();
    looseHealth.play();
}


void checkCollisions(){
  //Vérifie que le player n'est pas hors de l'écran (en bas)
  if(yPlayer > 630){
    ySpeed = 0;
  }
  //Vérifie que le player n'est pas hors de l'écran (en haut)
  if(yPlayer<0){
        dead();
  }

//Vérifie les collisions avec les murs
  for(int i = 0;i<3;i++){
    boolean hit = lineRect(w[i].xWall, w[i].yWall, w[i].xWall + cos(w[i].h) * w[i].radius, w[i].yWall + sin(w[i].h) * w[i].radius, xPlayer, yPlayer, rectWidth, rectHeight);
    if (hit){
     dead();
    }
  }
  
  //Vérifie les collisions avec les murs
  for(int i = 0;i<3;i++){
    boolean hit = lineRect(rw.xRotateWall, w[i].yWall, rw.xRotateWall + cos(w[i].h) * w[i].radius, w[i].yWall + sin(w[i].h) * w[i].radius, xPlayer, yPlayer, rectWidth, rectHeight);
    if (hit){
     dead();
    }
  }
  
//Vérifie les collisions avec les coin, si il y a collision elle disparait de l'écran
  for(int j = 0;j<8;j++){
    if ((c[j].xCoin > xPlayer && c[j].xCoin < xPlayer + rectWidth) && (c[j].yCoin + 30 > yPlayer && c[j].yCoin < yPlayer + rectHeight)  ) {
      c[j].xCoin = -30; 
      coin.rewind();
      coin.play();
      stopwatch += 500;
    }
  }
    
 // verifie les collisions avec le missile
  if ((pro.xProjectile > xPlayer && pro.xProjectile < xPlayer + rectWidth) && (pro.yProjectile> yPlayer && pro.yProjectile < yPlayer + rectHeight)  ) {
        dead();
      }
  
  
 // verifie les collisions avec le bonus
   if ((b.xBonus > xPlayer && b.xBonus < xPlayer + rectWidth) && (b.yBonus + 50> yPlayer && b.yBonus < yPlayer + rectHeight)) {
    b.xBonus = -50;
    bonus.rewind();
    bonus.play();
    health += 1;
    
  }
}

// LINE/RECTANGLE
boolean lineRect(float x1Wall, float y1Wall, float x2Wall, float y2Wall, float xPlayer, float yPlayer, float rectWidth, float rectHeight) {

  // check if the line has hit any of the rectangle's sides
  // uses the Line/Line function below
  boolean left =   lineLine(x1Wall,y1Wall,x2Wall,y2Wall, xPlayer,yPlayer,xPlayer, yPlayer+rectHeight);
  boolean right =  lineLine(x1Wall,y1Wall,x2Wall,y2Wall, xPlayer+rectWidth,yPlayer, xPlayer+rectWidth,yPlayer+rectHeight);
  boolean top =    lineLine(x1Wall,y1Wall,x2Wall,y2Wall, xPlayer,yPlayer, xPlayer+rectWidth,yPlayer);
  boolean bottom = lineLine(x1Wall,y1Wall,x2Wall,y2Wall, xPlayer,yPlayer+rectHeight, xPlayer+rectWidth,yPlayer+rectHeight);

  // if ANY of the above are true, the line
  // has hit the rectangle
  if (left || right || top || bottom) {
    return true;
  }
  return false;
}


// LINE/LINE
boolean lineLine(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {

  // calculate the direction of the lines
  float uA = ((x4-x3)*(y1-y3) - (y4-y3)*(x1-x3)) / ((y4-y3)*(x2-x1) - (x4-x3)*(y2-y1));
  float uB = ((x2-x1)*(y1-y3) - (y2-y1)*(x1-x3)) / ((y4-y3)*(x2-x1) - (x4-x3)*(y2-y1));

  // if uA and uB are between 0-1, lines are colliding
  if (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1) {

    // optionally, draw a circle where the lines meet
    float intersectionX = x1 + (uA * (x2-x1));
    float intersectionY = y1 + (uA * (y2-y1));
    fill(255,0,0);
    noStroke();
    ellipse(intersectionX, intersectionY, 20, 20);

    return true;
  }
  return false;
}
}
