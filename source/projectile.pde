public class projectile {
  int xProjectile = 1250;
  int yProjectile;
  float xSpeedProjectile;
  
  void drawProjectile(){
    stroke(238, 68, 51);
    fill(238, 68, 51);
    strokeWeight(30);
    line(xProjectile,yProjectile,xProjectile + 40, yProjectile);
  }
  
  
  void move(){ 
   xProjectile += -15;
   xSpeedProjectile += -15;
  }
  
  void checkPosition() {
    if (xProjectile < 0){
    xProjectile = 1250;
    }
  }
  
  void Signal() {
    int xSignal= 1190;
    strokeWeight(2);
    fill(255, 222, 31);
    noStroke();
    triangle(xSignal - 27, yProjectile + 14, xSignal - 14, yProjectile - 13, xSignal, yProjectile + 14);
    stroke(64);
    line(xSignal - 14,yProjectile - 3,xSignal - 14, yProjectile + 6);
    line(xSignal - 14,yProjectile + 10,xSignal - 14, yProjectile + 11);
  }
}
