import ddf.minim.*;
Minim minim;
AudioPlayer introMusic, gameMusic, clickMenu, jump, coin, bonus, projectile, alert, looseHealth, gameOver;


//creation of object
player p = new player();
wall[] w = new wall[3];
coin[] c = new coin[8];
projectile pro = new projectile();
bonus b = new bonus();
rotationWall rw = new rotationWall();

//add font
PFont police;

//add cp5 libraries for button and textfield
import controlP5.*;
ControlP5 cp5;
controlP5.Button playButton, leaderboardButton, quitButton, backButton, menuSurnameButton, restartButton, restartMenu, stopMenu;
controlP5.Textfield menuSurnameText;

//variable for display the different menus
boolean displayMenu = false;
boolean displayBack = false;
boolean displayMenuSurname = false;
boolean displayRestart = false;
boolean displayStop = false;

//variable to change menu
String state = "m";

//variable to store the name of the player
String surname;

//variable to store the score of the player
int score;

//variable for the different timing
int time = millis();
int timebis = millis();
int timeter = millis();
int timeSpeed = millis();
int timeRotationWall;
int timeMissile;
int timeBonus;
int frequenceProjectile = 30;
int startTime = 0;


//menu variables
int[] button1 = new int[4];
int[] button2 = new int[4];
int[] button3 = new int[4];
int r=191;
int g=215;
int g1=215;
int g2=215;
int g3=215;
int bl=44;


//leaderboard variables
int[] scores = new int[5];
String[] names = new String[scores.length];
Table table; 
String fileName = "leaderboard.csv";

//declaration of different images
PImage img, backgroundMenu, backgroundLeaderboard, backgroundGame,  backgroundRestart, setting;

//----------------------------------------------------

void setup(){
  //define the size of the windows
  size(1200, 700);
  //create the wall
  for(int i = 0;i<3;i++){
    w[i]=new wall(i);
  }
  //create the coin
  for(int j = 0;j<8;j++){
    c[j]=new coin(j);
  }
  //create the projectile
  pro = new projectile();
  //create a bonus
  b = new bonus();
  //create a rotation wall
  rw = new rotationWall();
  
  //for the music
  minim = new Minim(this);
  introMusic  = minim.loadFile ("intro.mp3");
  gameMusic  = minim.loadFile ("game.mp3");
  
  //for the sound effects
  clickMenu = minim.loadFile ("click_menu.wav");
  jump = minim.loadFile ("jump.wav");
  coin = minim.loadFile( "coin.wav");
  bonus = minim.loadFile( "bonus.wav");
  projectile = minim.loadFile( "projectile.wav");
  alert = minim.loadFile("alert.mp3");
  looseHealth = minim.loadFile("loose-health.wav");
  gameOver = minim.loadFile("game-over.wav");
  
  //volume management
  introMusic.setGain(-10);
  gameMusic.setGain(-10);
  clickMenu.setGain(20);
  jump.setGain(15);
  coin.setGain(20);
  projectile.setGain(30);
  bonus.setGain(10);
  gameOver.setGain(30);
  looseHealth.setGain(30);
  
  //setup the framerate to reduce bugs
  frameRate(60);
  
  //loading different images
  img = loadImage("logo.png");
  backgroundMenu = loadImage("background-menu.jpg");
  backgroundLeaderboard = loadImage("background-leaderboard.png");
  backgroundGame = loadImage("background-game.jpg");
  backgroundRestart = loadImage("background-restart.png");
  setting = loadImage("setting.png");

  
  cp5 = new ControlP5(this);
      
      //Creation of the different button
      
      playButton = cp5.addButton("Play")
        .setPosition(195, 425)
        .setSize(250, 50)
        .setValue(100)
        .setColorBackground(color(184, 207, 43))
        .setColorForeground(color(164, 187, 23))
        .setColorActive(color(0,0,0))
        .addCallback(new CallbackListener() {
          public void controlEvent(CallbackEvent event) {
            if (event.getAction() == ControlP5.ACTION_RELEASED) {
              clickMenu.rewind();
              clickMenu.play();
              state = "ms";
              reset();
              println(state);
            }
          }
        }
      )
      ;
      
      leaderboardButton = cp5.addButton("Leaderboard")
        .setPosition(450, 425)
        .setSize(250, 50)
        .setValue(100)
        .setColorBackground(color(184, 207, 43))
        .setColorForeground(color(164, 187, 23))
        .setColorActive(color(0,0,0))
        .addCallback(new CallbackListener() {
          public void controlEvent(CallbackEvent event) {
            if (event.getAction() == ControlP5.ACTION_RELEASED) {
              clickMenu.rewind();
              clickMenu.play();
              state = "l";
              println(state);
            }
          }
        }
      )
      ;
      
      quitButton = cp5.addButton("Quit")
        .setPosition(705, 425)
        .setSize(300, 50)
        .setValue(100)
        .setColorBackground(color(184, 207, 43))
        .setColorForeground(color(164, 187, 23))
        .setColorActive(color(0,0,0))
        .addCallback(new CallbackListener() {
          public void controlEvent(CallbackEvent event) {
            if (event.getAction() == ControlP5.ACTION_RELEASED) {
              clickMenu.rewind();
              clickMenu.play();
              state = "q";
              println(state);
            }
          }
        }
      )
      ;
      
      backButton = cp5.addButton("Back")
          .setPosition(60, 40)
          .setSize(250, 50)
          .setValue(100)
          .setColorBackground(color(184, 207, 43))
          .setColorForeground(color(164, 187, 23))
          .setColorActive(color(0,0,0))
          .addCallback(new CallbackListener() {
            public void controlEvent(CallbackEvent event) {
              if (event.getAction() == ControlP5.ACTION_RELEASED) {
                clickMenu.rewind();
                clickMenu.play();
                state = "m";
                println(state);
              }
            }
          }
        )
        ;
        
      menuSurnameButton = cp5.addButton("Let's go!")
          .setPosition(475, 365)
          .setSize(300, 50)
          .setValue(100)
          .setColorBackground(color(184, 207, 43))
          .setColorForeground(color(164, 187, 23))
          .setColorActive(color(0,0,0))
          .addCallback(new CallbackListener() {
            public void controlEvent(CallbackEvent event) {
              if (event.getAction() == ControlP5.ACTION_RELEASED) {
                clickMenu.rewind();
                clickMenu.play();
                state = "p";
                gameMusic.rewind();
                introMusic.rewind();
                startTime = millis();
                println(state);
              }
            }
          }
        )
        ;
        
      menuSurnameText = cp5.addTextfield("Surname")
         .setPosition(475,285)
         .setSize(300,60)
         .setColor(color(0,0,0))
         .setColorActive(color(161,175,14)) 
         .setColorBackground(color(211,235,64)) 
         ;
         
      restartButton = cp5.addButton("Restart")
        .setPosition(270, 420)
          .setSize(300, 50)
          .setValue(100)
          .setColorBackground(color(184, 207, 43))
          .setColorForeground(color(164, 187, 23))
          .setColorActive(color(0,0,0))
          .addCallback(new CallbackListener() {
            public void controlEvent(CallbackEvent event) {
              if (event.getAction() == ControlP5.ACTION_RELEASED) {
                clickMenu.rewind();
                clickMenu.play();
                state = "p";
                startTime = millis();
                gameMusic.rewind();
                introMusic.rewind();
                reset();
                println(state);
              }
            }
          }
        )
      ;
      
      restartMenu = cp5.addButton("Menu")
          .setPosition(590, 420)
          .setSize(300, 50)
          .setValue(100)
          .setColorBackground(color(184, 207, 43))
          .setColorForeground(color(164, 187, 23))
          .setColorActive(color(0,0,0))
          .addCallback(new CallbackListener() {
            public void controlEvent(CallbackEvent event) {
              if (event.getAction() == ControlP5.ACTION_RELEASED) {
                clickMenu.rewind();
                clickMenu.play();
                state = "m";
                gameMusic.rewind();
                introMusic.rewind();
                println(state);
              }
            }
          }
        )
      ;
      
      //loading of the font
      police = loadFont("Lato-Bold-48.vlw");
        
      // LEADER BOARD //
      
      //creation of the leaderboard table
      table = null;
      table = loadTable(fileName, "header");
      
    
      //If a table already exist, we use that
      if (table!=null) { 
     
      for (TableRow row : table.rows()) { 
          int id = row.getInt("id");
          names[id] = row.getString("name");
          scores[id] = row.getInt("HighScore");
        }
        //If a file cannot be found, we create one!
      } else { 
        table = new Table();  
        table.addColumn("id"); 
        table.addColumn("name");
        table.addColumn("HighScore");
   
        for (int i = 0; i<scores.length; i++) {  
          TableRow newRow = table.addRow();  
          newRow.setInt("id", table.lastRowIndex());
          newRow.setString("name", names[i]);
          newRow.setInt("HighScore", scores[i]);
        }
  }

}

//----------------------------------------------------

void draw(){
  background(64,64,64);
  
  switch (state) {
    //m=menu, ms = menu surname p=play, l=leaderboard, r = restart menu, rv = menu just when the player loose a life ,q=quit
    
    case "m" :
    // add the background and the logo 
        backgroundMenu.resize(1200,700);
        image(backgroundMenu,0,0);
        image(img,370,70);
        img.resize(400,400);
        
    //define which menu i want to display
        displayMenu = true;
        displayBack = false;
        displayMenuSurname = false;
        displayRestart = false;
        hideButtonRestart();
        hideMenuSurname();
        hideButtonMenu();
        hideButtonLeaderboard();
        
    //manage the music for the menu and the sound effect
        introMusic.play();
        gameMusic.pause();
      break;
      
    case "ms":
      // add the background
      backgroundMenu.resize(1200,700);
      image(backgroundMenu,0,0);
      
      //define which menu i want to display
      displayMenuSurname = true;
      displayMenu = false;
      hideMenuSurname();
      hideButtonMenu();
      displayRestart = false;
      hideButtonRestart();
      //store the content of the textfield in the varible surname
      surname = cp5.get(Textfield.class,"Surname").getText();
      
      //manage the music for the menu and the sound effect
      introMusic.play();
      break;
      
      
    case "p" :
    // add the background
      backgroundGame.resize(1200,700);
      image(backgroundGame,0,0);
    //define which menu i want to display
      displayMenuSurname = false;
      hideMenuSurname();
      displayMenu = false;
      displayBack = false;
      hideButtonLeaderboard();
      hideButtonMenu();
      displayRestart = false;
      hideButtonRestart();
      gameOver.rewind();

   if (millis() - timeSpeed > 2000)
      {
       if (p.xSpeed < 15 && frequenceProjectile < 40)
       {
         p.xSpeed += 0.2;
         pro.xSpeedProjectile += 0.2;
         rw.xSpeedRotateWall -= 0.2;
         frequenceProjectile -= 5;
         timeSpeed = millis(); 
       }
      }
        p.move();
        p.drag();
        p.drawPlayer();
        p.checkCollisions();
        
        //music
        introMusic.pause();
        gameMusic.play();
        
        //stopwatch
        
        p.stopwatch = millis() - startTime;
       

        
        for(int j = 0;j<8;j++)
        { 
          c[j].drawCoin();
          c[j].checkPosition();
        }
        
        //Missile
         timeMissile = (int) random(10, frequenceProjectile) * 1000;
        
        if (millis() - time < 1000)
        {
         pro.Signal();
         alert.play();
        }
        
        if ((1500 <millis() - time ) && (millis() - time < 6000))
        {
         pro.drawProjectile();
         pro.move();
         projectile.play();
         alert.rewind();
        }
      
        if (millis() - time > timeMissile)
        {
         time = millis(); 
         pro.checkPosition();
         pro.yProjectile = (int) random(75, 675);
         projectile.rewind(); 
        }
        
         //Rotation Wall
         timeRotationWall = (int) random(9, 12) * 1000;
      
        
        if ((1500 <millis() - timeter ) && (millis() - timeter < 7000))
        {
         rw.drawRotationWall();
         rw.move();
        }
      
        if (millis() - timeter > timeRotationWall)
        {
         timeter = millis(); 
         rw.checkPosition();
         rw.yRotateWall = (int(random(50,650)));
        }
        
          //Bonus
         timeBonus = (int) random(30, 70) * 1000;
      
        
        if ((1500 <millis() - timebis ) && (millis() - timebis < 7000))
        {
         b.drawBonus();
         b.move();
        }
      
        if (millis() - timebis > timeBonus)
        {
         timebis = millis(); 
         b.checkPosition();
         b.yBonus = (int(random(50,650)));
        }
        
        //draw floor
        stroke(255);
        strokeWeight(20);
        line(0,695, 1250, 695); 
      
        // head menu 
        noStroke();
        fill (255);
        rect(0, 0, 1250, 50);
        //score
        
        fill(0);
        textFont(police, 30);
        text("Score : ", 50, 35);
        text(p.stopwatch, 150, 35);
        
        fill(0);
        textFont(police, 30);
        text("Health : ", 300, 35);
        text(p.health, 400, 35);
        
        fill(0);
        textFont(police, 30);
        text("Surname : ", 900, 35);
        text(surname, 1045, 35);
        
              
        for(int i = 0;i<3;i++)
        {
          w[i].drawWall();
          w[i].checkPosition();
        }
        println(state);
        
 
        break;
        
        
      case "l" :
//When a new number is generated, we want to add it to the table as well, I think.
        backgroundMenu.resize(1200,700);
        introMusic.play();
        image(backgroundMenu,0,0);
        textFont(police, 30);
        image(backgroundLeaderboard,0,0);
        fill(80);
        for (int i=0; i<scores.length; i++) {
          textSize(57);
          text(scores[0], 680, 205);
          textSize(55);
          text(scores[1], 680, 330);
          textSize(50);
          text(scores[2], 680, 435);
          textSize(40);
          text(scores[3], 680, 530);
          textSize(40);
          text(scores[4], 680, 630);
          if(names[i] != null)
          {
            textSize(57);
            text(names[0], 270, 205);
            textSize(55);
            text(names[1], 270, 330);
            textSize(50);
            text(names[2], 270, 435);
            textSize(40);
            text(names[3], 270, 530);
            textSize(40);
            text(names[4], 270, 630);
          }
        }

        displayMenu = false;
        displayBack = true;
        hideButtonLeaderboard();
        hideButtonMenu();
        displayRestart = false;
        hideButtonRestart();
        break;
     
     case "r":
       backgroundMenu.resize(1200,700);
       image(backgroundMenu,0,0);
       backgroundRestart.resize(1200,700);
       image(backgroundRestart,0,0);
       fill(48);
       textAlign(CENTER, CENTER);
       textSize(30);
       text(surname, 420, 360);
       text(p.stopwatch, 740, 360);
       displayRestart = true;
       hideButtonRestart();
       gameMusic.pause();
       gameOver.play();
       introMusic.play();
       break;
     
     case "rv":
       state = "p";
       break;
       
     
     case "q" :
       displayMenu = false;
       hideButtonMenu();
       exit();
       break;
       

     
  }
}

//----------------------------------------------------
void resetScore(){
  
}

void reset(){

 
 startTime = millis();
 p.yPlayer=400;

 for(int i = 0;i<3;i++){
  w[i].xWall+=1400; // distance in the start for the wall
 }
 
 for(int j = 0; j<8; j++){
  c[j].xCoin+=3000; // distance in the start for the coin
 }
 
 p.xSpeed = 3;
 pro.xSpeedProjectile = 15;
 frequenceProjectile = 30;
 p.health = 1;
 
 if (state != "rv"){
  p.stopwatch = 0; 
 }
}


//----------------------------------------------------

void keyPressed(){
  if(state == "p")
  {
  jump.rewind();
  jump.play();
  p.jump();
  }
  

}

//----------------------------------------------------

void hideButtonMenu() {
  
  if (displayMenu == false){
    playButton.hide();
    leaderboardButton.hide();
    quitButton.hide();
  }
  else if (displayMenu == true){
    playButton.show();
    leaderboardButton.show();
    quitButton.show();
  }
}

//----------------------------------------------------
void hideMenuSurname() {
  
  if (displayMenuSurname == false){
    menuSurnameButton.hide();
    menuSurnameText.hide();
  }
  else if (displayMenuSurname == true){
    menuSurnameButton.show();
    menuSurnameText.show();
  }
}

//----------------------------------------------------

void hideButtonLeaderboard() {
  
  if (displayBack == false){
    backButton.hide();
  }
  else if (displayBack == true){
    backButton.show();
  }
}
//----------------------------------------------------
void hideButtonRestart() {
  if (displayRestart == false) {
     restartButton.hide();
     restartMenu.hide();
  }
  else if (displayRestart == true) {
     restartButton.show(); 
     restartMenu.show();
  }
}

//----------------------------------------------------

void hideMenuStop() {
  if (displayStop == false) {
     restartButton.hide();
     stopMenu.hide();
  }
  else if (displayStop == true) {
     restartButton.show(); 
     stopMenu.show();
  }
}

//----------------------------------------------------

void addNewScore(int score) {
  for (int i=0; i<scores.length; i++) {
    if (score>=scores[i]) {
      for (int j=scores.length-1; j>=max(i, 1); j--) {
        scores[j] = scores[j-1];
        names[j] = names[j-1];
      }
      scores[i] = score;
      names[i] = surname;
      
      addData();
      saveT();
      break;
    }
  }
}

//----------------------------------------------------

void addData() {  //This function should now really be called "updateTable" or something
  for (TableRow row : table.rows()) {  //For every row, update the content from the array
    int id = row.getInt("id");
    table.setString(id, "name", names[id]);
    table.setInt(id, "HighScore", scores[id]);
  }
}

//----------------------------------------------------
 
void saveT() {
  //save the table
  saveTable(table, "data/leaderboard.csv");
}
